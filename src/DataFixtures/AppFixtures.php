<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
          for ($i = 0; $i < 10; $i++) {
            $person = new Person();
            $person->setName('Valentine Mathy'.$i);
            $person->setDescription('Responsable recrutement & formation @Projet & Performances (Région PACA)'.$i);
            $person->setImagePath("https://www.duchess-france.org/wp-content/uploads/2019/06/mypic_cover_twitter-270x260.jpg");
            $person->setIsFrench($i%2 == 0);

            $manager->persist($person);
        

        $manager->flush();
    }
}
}