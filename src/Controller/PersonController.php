<?php

namespace App\Controller;

use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PersonController extends AbstractController
{
    /**
     * @Route("/", name="person")
     */
    public function index(PersonRepository $repo)
    {
        return $this->render('person/index.html.twig', [
            'persons' => $repo->findAll()
        ]);
    }
}